import numpy as np


def gauss_seidel(A, b, epsilon=1e-8, max_iter=100):
    n = len(b)
    x = np.zeros(n)

    for k in range(max_iter):
        x_new = np.copy(x)
        for i in range(n):
            sum_1 = np.dot(A[i, :i], x_new[:i])
            sum_2 = np.dot(A[i, i + 1:], x[i + 1:])
            x_new[i] = (b[i] - sum_1 - sum_2) / A[i, i]

        error = np.linalg.norm(x_new - x, ord=np.inf)
        if error < epsilon:
            print(f"Đạt được độ chính xác sau {k + 1} lần lặp.")
            return x_new

        x = np.copy(x_new)

    print("Không đạt được độ chính xác sau số lần lặp tối đa.")
    return None


if __name__ == "__main__":
    # Đọc ma trận A và vector b từ file input.txt (hoặc có thể nhập trực tiếp dữ liệu)
    A = np.loadtxt("input.txt")
    b = np.loadtxt("vector_b.txt")

    # Độ chính xác cho phép và số lần lặp tối đa
    epsilon = 1e-8
    max_iter = 100

    # Giải phương trình Ax = b bằng phương pháp Gauss-Seidel
    solution = gauss_seidel(A, b, epsilon, max_iter)
    if solution is not None:
        print("Nghiệm gần đúng:")
        print(solution)
