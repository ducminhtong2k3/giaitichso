import numpy as np
from scipy.optimize import brentq


def ff(x):
    return x ** 4 + 3 * x ** 3 - 11 * x ** 2 - 3 * x + 10


def find_roots(a, b):
    roots = []
    while a <= b:
        root = brentq(ff, a, b)
        roots.append(root)
        a = root + 0.01  # You can adjust the step size here

    return roots


def main():
    n = int(input("Nhap bac cua da thuc: "))
    a = []
    print("Nhap cac he so:")
    for j in range(n + 1):
        a.append(float(input()))

    count1, count2 = 0, 0
    k1, k2 = None, None
    B1, B2 = None, None
    N1, N2 = None, None

    for i in range(1, n + 1):
        if a[i] < 0:
            k1 = i
            break

    for i in range(1, n + 1):
        if a[i] < 0:
            count1 += 1

    for i in range(1, n + 1):
        if a[i] < 0:
            B1 = a[i]
            for j in range(1, n + 1):
                if a[j] < 0:
                    if -a[j] > -B1:
                        B1 = a[j]

    N1 = 1 + np.exp((1.0 / k1) * np.log(-B1 / a[0]))

    for i in range(n + 1):
        a[i] = -a[i]

    for i in range(1, n + 1):
        if a[i] < 0:
            k2 = i
            break

    for i in range(1, n + 1):
        if a[i] < 0:
            count2 += 1

    for i in range(1, n + 1):
        if a[i] < 0:
            B2 = a[i]
            for j in range(1, n + 1):
                if a[j] < 0:
                    if -a[j] > -B2:
                        B2 = a[j]

    N2 = 1 + np.exp((1.0 / k2) * np.log(-B2 / a[0]))

    if count1 == 0:
        N1 = 0

    if count2 == 0:
        N2 = 0

    print("\nKhoang chua nghiem cua phuong trinh la (%f, %f)" % (-N2, N1))

    roots = find_roots(-N2, N1)

    for root in roots:
        print("Root:", root)


if __name__ == "__main__":
    main()
