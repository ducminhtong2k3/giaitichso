# GiaiTichSo

## Giải gần đúng phương trình f(x) = 0

- PP_ChiaDoi
- PP_LapDon
- PP_DayCung
- PP_TiepTuyen

## Giải "gần đúng" hệ phương trình

- PP_LapDon
- PP_TiepTuyen

## Giải "đúng" hệ A.X = B

- PP_Gauss
- PP_GaussJordan (nên dùng)

## Giải "gần đúng" hệ A.X = B

- PP_LapDon
- PP_Jacobi: A phải là ma trận chéo trội (hàng/cột)
- PP_Seidel
- PP_GaussSeidel: A phải là ma trận chéo trội (hàng/cột)

## Giải "đúng" ma trận nghịch đảo
A là ma trận vuông

- PP_GaussJordan
- PP_Cholesky: A phải là ma trận đối xứng
- PP_VienQuanh

## Giải "gần đúng" ma trận nghịch đảo
A là ma trận vuông

- PP_Jacobi
- PP_Seidel
- PP_Newton

## Giá trị riêng, vector riêng
A là ma trận vuông
- PP_Danielevski

## Giá trị riêng trội

- PP_LuyThua
- PP_XuongThang

## Giá trị kỳ dị, vector kỳ dị, SVD

- PP_SVD
