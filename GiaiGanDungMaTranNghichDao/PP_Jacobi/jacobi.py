import numpy as np


def jacobi_inverse(matrix, iterations=100, tolerance=1e-8):
    n = matrix.shape[0]
    X = np.zeros_like(matrix)
    X_new = np.zeros_like(matrix)

    for k in range(iterations):
        for i in range(n):
            X_new[i, i] = 1 / matrix[i, i]
            for j in range(n):
                if i != j:
                    X_new[i, j] = -matrix[i, j] / matrix[i, i]

        if np.linalg.norm(X_new - X) < tolerance:
            break

        X = np.copy(X_new)

        print(f"Iteration {k + 1}:")
        print(X)

    return X


# Hàm để đọc ma trận từ file văn bản
def read_matrix_from_file(filename):
    with open(filename, 'r') as file:
        lines = file.readlines()
        matrix_data = [list(map(float, line.split())) for line in lines]
        matrix = np.array(matrix_data)
        return matrix


# Đường dẫn của tệp chứa ma trận
file_path = "input.txt"

# Đọc ma trận từ file
try:
    A = read_matrix_from_file(file_path)
except FileNotFoundError:
    print(f"File '{file_path}' not found.")
    exit(1)
except ValueError:
    print("Invalid matrix data in the file.")
    exit(1)

# Tìm gần đúng ma trận nghịch đảo
A_inv = jacobi_inverse(A)
print("Approximate inverse matrix:")
print(A_inv)
