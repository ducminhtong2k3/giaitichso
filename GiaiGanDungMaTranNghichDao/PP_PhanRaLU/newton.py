import numpy as np


def lu_decomposition_inverse(A, tolerance=1e-2):
    n = A.shape[0]
    P, L, U = lu_decomposition(A)

    # Tìm ma trận nghịch đảo của U với sai số tolerance
    U_inv = np.linalg.inv(U + np.eye(n) * tolerance)

    # Tìm ma trận nghịch đảo của A
    A_inv = np.dot(U_inv, np.dot(np.linalg.inv(L), P.T))

    return A_inv


def lu_decomposition(A):
    n = A.shape[0]
    P = np.eye(n)
    L = np.eye(n)
    U = A.copy()

    for i in range(n):
        # Tìm chỉ số hàng có giá trị tuyệt đối lớn nhất trong cột i
        max_row = np.argmax(np.abs(U[i:, i])) + i

        # Hoán đổi hàng max_row với hàng i
        if max_row != i:
            U[[i, max_row], :] = U[[max_row, i], :]
            P[[i, max_row], :] = P[[max_row, i], :]
            L[[i, max_row], :i] = L[[max_row, i], :i]

        pivot = U[i, i]

        # Tạo các giá trị phần tử của ma trận L
        L[i + 1:, i] = U[i + 1:, i] / pivot

        # Thực hiện phép biến đổi Gaussian trên các hàng còn lại của ma trận U
        for j in range(i + 1, n):
            U[j, :] -= L[j, i] * U[i, :]

    return P, L, U


# Hàm để đọc ma trận từ file văn bản
def read_matrix_from_file(filename):
    try:
        with open(filename, 'r') as file:
            lines = file.readlines()
            matrix_data = [list(map(float, line.split())) for line in lines]
            matrix = np.array(matrix_data)
            return matrix
    except FileNotFoundError:
        print(f"File '{filename}' not found.")
        exit(1)
    except ValueError:
        print("Invalid matrix data in the file.")
        exit(1)


# Đường dẫn của tệp chứa ma trận
file_path = "input.txt"

# Đọc ma trận từ file
A = read_matrix_from_file(file_path)

# Tìm gần đúng ma trận nghịch đảo
A_inv_approx = lu_decomposition_inverse(A)
print("Approximate inverse matrix:")
print(A_inv_approx)
