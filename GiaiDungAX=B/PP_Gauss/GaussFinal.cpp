// Gauss giải HPT AX = B

#include<cstdio>

// Gói nhập ma trận

// Gauss_Test1: hệ có nghiệm duy nhất
// Gauss_Test2: hệ có vô số nghiệm
// Gauss_Test3: hệ vô nghiệm

// Nhập ma trận A trước rồi nhập vecto B

void importMatrix(double a[][100], int m, int n){
    FILE *fp;
    fp = fopen("Gauss_Test1.txt", "r");

	// Đọc ma trận A
	for(int i = 1; i <= m; i++){
        for(int j = 1; j <= n ; j++){
            fscanf(fp, "%lf", &a[i][j]);
        }
    }
	// Đọc vecto B
	for (int i = 1; i <= n; i++){
		fscanf(fp, "%lf", &a[i][n + 1]);
	}
	fclose(fp);

    // In ma trận vừa nhập
    printf("\nMa tran vua nhap la:\n");
    for(int i = 1; i <= m; i++){
        for(int j = 1; j <= n + 1 ; j++){
            printf("%lf ", a[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

// Gói đổi chỗ hàng i cho hàng j ở ma trận bổ sung a
// n là số cột của ma trận A
void swap(double a[][100], int i, int j, int n){
	for(int k = 1; k <= n + 1; k++){
		double t = a[i][k];
		a[i][k] = a[j][k];
		a[j][k] = t;
	}
}

// Gói tìm giá trị max giữa a và b
int max(int a, int b){
	if(a > b) return a;
	else return b;
}

// Phương pháp Gauss giải HPT AX = B với ma trận bổ sung là a
// Ma trận A có m hàng, n cột
void Gauss(double a[][100], int m, int n){
	// index[]: dùng để lưu chỉ số hàng bằng chỉ số cột
	// count: đếm số hàng khác không
	// dd[]: lưu vị trí tham số khi HPT có vô số nghiệm
	int index[100], max_index =-999, count = 0, dd[100];
	for(int i = 1; i <= m; i++){
		index[i] = 0;
	}
	for(int i = 1; i <= n; i++){
		dd[i] = 0;
	}

	int i = 1, j = 1;
	double q;
	while(i <= m && j <= n + 1){
		// Đổi thứ tự các phương trình
		while(a[i][j] == 0 && i <= m && j <= n + 1){
			int t = i;
			while(a[t][j] == 0 && t <= m){
				t++;
				if(t <= m){
					swap(a, t, i, n);
				}
				if(t > m){
					j++;
				}
			}
		}
		// Đưa về ma trận bậc thang
		if(a[i][j] != 0){
			for(int k = i + 1; k <= m; k++){
				q = -1 * a[k][j] / a[i][j];
				for(int l = j; l <= n + 1; l++){
					a[k][l] += q * a[i][l];
				}
			}
			// In ra ma trận sau mỗi lần khử cột
			if (i < m && j < n){
				printf("Ma tran sau lan khu thu %d la:\n", j);
				for(int i = 1; i <= m; i++){
        			for(int j = 1; j <= n + 1; j++){
            			printf("%lf ", a[i][j]);
        			}
        			printf("\n");
    			}
    			printf("\n");
			}
			index[i] = j;
			if(i <= m && j <= n) count++;
			max_index = max(max_index, index[i]);
			dd[j] = 1;
		}
		i++;
		j++;
	}

	// Biện luận
	// Trường hợp hệ phương trình vô nghiệm
	if(max_index > n){
		printf("He phuong trinh vo nghiem\n");
	}

	// Trường hơp hệ phương trình có nghiệm duy nhất
	if(index[m] == n && count == n){
		printf("\nHe phuong trinh co nghiem duy nhat\n");
        printf("\nNghiem cua he phuong trinh la:\n");
		double x[100];
		for(int i = m; i >= 1; i--){
			x[i] = a[i][n + 1] / a[i][i];
			for(int j = n; j > i; j--){
				x[i] -= a[i][j] * x[j] / a[i][i];
			}
		}
		for(int i = 1; i <= m; i++){
			printf("x%d = %lf\n",i,x[i]);
		}
	}

	// Trường hợp hệ phương trình có vô số nghiệm
	if(max_index <= n && count < n){
		printf("\nHe phuong trinh co vo so nghiem phu thuoc vao %d tham so\n", m - count);
		printf("\nNghiem cua he phuong trinh la:\n");
		for(int i1 = count; i1 >= 1; i1--){
			for(int k = i1 - 1; k >= 1; k--){
				q = -1 * a[k][index[i1]] / a[i1][index[i1]];
				for(int l = index[i1]; l <= n + 1; l++){
					a[k][l] += q * a[i1][l];
				}
			}
		}
		for(int i1 = 1; i1 <= count; i1++){
			printf("x%d = %lf", index[i1], a[i1][n+1] / a[i1][index[i1]]);
			for(int k = 1; k <= n; k++){
				if(dd[k] == 0){
					if(a[i1][k] / a[i1][index[i1]] > 0){
						printf(" - %lf x%d", a[i1][k] / a[i1][index[i1]], k);
					}
					if(a[i1][k] / a[i1][index[i1]] < 0){
						printf(" + %lf x%d", - 1 * a[i1][k] / a[i1][index[i1]], k);
					}
				}
			}
			printf("\n");
		}
		for (int k = 1; k <= n; k++){
			if (dd[k] == 0){
				printf("x%d = x%d", k, k);
			}
		}
	}
}
int main(){
	double  a[100][100];
	int m, n;

	// Nhập số phương trình và số ẩn
	printf("Nhap so phuong trinh va so an \n");
	printf("So phuong trinh: "); scanf("%d", &m);
	printf("So an: "); scanf("%d", &n);

	importMatrix(a, m, n);

	Gauss(a, m, n);
	return 0;
}
