import numpy as np


def cholesky_inverse(A):
    n = A.shape[0]
    L = np.linalg.cholesky(A)  # Tính ma trận Cholesky L

    # Tạo ma trận tam giác dưới L_transpose
    L_transpose = L.T
    # Tạo ma trận nghịch đảo của L
    L_inv = np.linalg.inv(L)

    # Tính ma trận nghịch đảo của A
    A_inv = L_inv.T @ L_inv

    return A_inv


def read_matrix_from_file(file_path):
    matrix = np.loadtxt(file_path)
    return matrix


# Đọc ma trận A từ file txt
file_path = "input.txt"
A = read_matrix_from_file(file_path)

# In ma trận A
print("Ma trận A:")
print(A)

# Tính ma trận nghịch đảo
A_inv = cholesky_inverse(A)

# In kết quả trung gian
print("Ma trận Cholesky L:")
print(np.linalg.cholesky(A))
print("Ma trận chuyển vị của L:")
print(np.linalg.cholesky(A).T)
print("Ma trận nghịch đảo của L:")
print(np.linalg.inv(np.linalg.cholesky(A)))

# In ma trận nghịch đảo
print("Ma trận nghịch đảo của A:")
print(A_inv)
