import numpy as np


def gauss_jordan_inverse(matrix):
    """
    Dùng cho ma trận vuông

    :param matrix:
    :return:
    """
    n = matrix.shape[0]
    augmented_matrix = np.concatenate((matrix, np.eye(n)), axis=1)

    for i in range(n):
        pivot = augmented_matrix[i][i]

        if pivot == 0:
            raise ValueError("The matrix is not invertible.")

        augmented_matrix[i] /= pivot

        print(f"Iteration {i + 1}:")
        print(augmented_matrix)

        for j in range(n):
            if i != j:
                factor = augmented_matrix[j][i]
                augmented_matrix[j] -= factor * augmented_matrix[i]

    inverse_matrix = augmented_matrix[:, n:]
    return inverse_matrix


# Hàm để đọc ma trận từ file txt
def read_matrix_from_file(filename):
    with open(filename, 'r') as file:
        lines = file.readlines()
        matrix_data = [list(map(float, line.split())) for line in lines]
        matrix = np.array(matrix_data)
        return matrix


# Đường dẫn của tệp chứa ma trận
file_path = "input.txt"

# Đọc ma trận từ file
try:
    A = read_matrix_from_file(file_path)
except FileNotFoundError:
    print(f"File '{file_path}' not found.")
    exit(1)
except ValueError:
    print("Invalid matrix data in the file.")
    exit(1)

# Kiểm tra khả nghịch của ma trận
if np.linalg.det(A) == 0:
    print("The matrix is not invertible.")
    exit(1)

# Tìm ma trận nghịch đảo
try:
    A_inv = gauss_jordan_inverse(A)
except ValueError as e:
    print(str(e))
    exit(1)

print("Inverse matrix:")
print(A_inv)
